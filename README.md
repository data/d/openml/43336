# OpenML dataset: Complete-Cryptocurrency-Market-History

https://www.openml.org/d/43336

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Cryptocurrencies
Cryptocurrencies are fast becoming rivals to traditional currency across the world. The digital currencies are available to purchase in many different places, making it accessible to everyone, and with retailers accepting various cryptocurrencies it could be a sign that money as we know it is about to go through a major change.
In addition, the blockchain technology on which many cryptocurrencies are based, with its revolutionary distributed digital backbone, has many other promising applications. Implementations of secure, decentralized systems can aid us in conquering organizational issues of trust and security that have plagued our society throughout the ages. In effect, we can fundamentally disrupt industries core to economies, businesses and social structures, eliminating inefficiency and human error.
Content
The dataset contains all historical daily prices (open, high, low, close) for all cryptocurrencies listed on CoinMarketCap.
Acknowledgements

Every Cryptocurrency Daily Market Price - I initially developed kernels for this dataset before making my own scraper and dataset so that I could keep it regularly updated.
CoinMarketCap  - For the data

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43336) of an [OpenML dataset](https://www.openml.org/d/43336). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43336/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43336/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43336/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

